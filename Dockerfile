# Example: docker build . -t dsvw && docker run -p 65412:65412 dsvw

FROM alpine:3.11

RUN apk --no-cache add git python3 py-lxml \
    && rm -rf /var/cache/apk/*

ADD . .

RUN sed -i 's/127.0.0.1/0.0.0.0/g' src/dsvw.py
RUN echo Damn Vulnerable app is running port 65412
EXPOSE 65412

CMD ["python3", "src/dsvw.py"]

